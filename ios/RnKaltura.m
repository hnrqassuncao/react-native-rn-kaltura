#import "RnKaltura.h"
#import <MapKit/MapKit.h>

@implementation RnKaltura

RCT_EXPORT_MODULE(RNTMap)

- (UIView *)view
{
  return [[MKMapView alloc] init];
}

@end
